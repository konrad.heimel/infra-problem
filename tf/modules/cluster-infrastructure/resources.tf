resource "kubernetes_manifest" "clusterissuer" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1",
    "kind" = "ClusterIssuer",
    "metadata" = {
      "name" = "letsencrypt"
    },
    "spec" = {
      "acme" = {
        "server" = "https://acme-v02.api.letsencrypt.org/directory",
        "email" = "konrad.heimel@mt-ag.com",
        "privateKeySecretRef" = {
          "name" = "letsencrypt"
        },
        "solvers" = [{
          "http01" = {
            "ingress" = {
              "class" = "nginx",
              "podTemplate" = {
                "spec" = {
                  "nodeSelector" = {
                    "kubernetes.io/os" = "linux"
                  }
                }
              }
            }
          }
        }]
      }
    }
  }
  depends_on = [
    helm_release.cert_manager,
    helm_release.ingress_nginx
  ]
}
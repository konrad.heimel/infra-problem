terraform {
    required_providers {
        azurerm = {
            source = "hashicorp/azurerm"
        }
        random = {
            source = "hashicorp/random"
        }
        kubernetes = {}
        helm = {}
    }
    required_version = ">= 0.13"
}

provider "azurerm" {
    features {}
    skip_provider_registration = true
}



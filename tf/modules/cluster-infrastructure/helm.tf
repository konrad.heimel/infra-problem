resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  namespace = "ingress-nginx"
  create_namespace = true
  chart            = "ingress-nginx"
  values = [
    "${file("${path.module}/files/values-nginx.yaml")}"
  ]
  depends_on = [
    helm_release.kube_prometheus_stack
  ]
}

resource "helm_release" "kube_prometheus_stack" {
  name             = "kube-prometheus-stack"
  repository = "https://prometheus-community.github.io/helm-charts"
  namespace = "monitoring"
  create_namespace = true
  chart            = "kube-prometheus-stack"
  values = [
    "${file("${path.module}/files/values-kube-prometheus-stack.yaml")}"
  ]
}


resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  repository = "https://charts.jetstack.io"
  namespace = "cert-manager"
  create_namespace = true
  chart            = "cert-manager"
  values = [
    "${file("${path.module}/files/values-cert-manager.yaml")}"
  ]
}

resource "helm_release" "argocd" {
  name             = "argocd"
  repository = "https://argoproj.github.io/argo-helm"
  namespace = "argocd"
  create_namespace = true
  chart            = "argo-cd"
  values = [
    "${file("${path.module}/files/values-argocd.yaml")}"
  ]
}
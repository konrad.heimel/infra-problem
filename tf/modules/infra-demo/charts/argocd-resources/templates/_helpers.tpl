{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chart.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}
eyJhbGciOiJSUzI1NiIsImtpZCI6ImRwd2RXdXdZY0I5OGxhVkJ0enVLSUVxbkczZ1ctTkZxb0czR0NQTGJpMTAifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJpc3Rpby1zeXN0ZW0iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlY3JldC5uYW1lIjoia2lhbGktc2VydmljZS1hY2NvdW50LXRva2VuLTl0N3h6Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImtpYWxpLXNlcnZpY2UtYWNjb3VudCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjE3YzBhOTMyLTk1MjktNGE2OS1hMThiLWQyZTIzZjhlZmJiNCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDppc3Rpby1zeXN0ZW06a2lhbGktc2VydmljZS1hY2NvdW50In0.c_c8I4-OKULk0pEBnGtM5urO07BY0xlTr6OPDlQvRwTtRsJeL-Y1ozHPjzhKv2Hw6zMoSr7ujxz3SXC0XbI-atsKgruULfinoU6FwRE474uHvCJov-viZipcAICnWq7RfauAWuUf5we3RCPCj5ULn5AoBqg05nF7d1MhctLJDcyzRiSmZmWgEbEXa1l1C7Rq0X_z5OhCbJGRduyltQGZZfYg1ojE1zzc9NEMXueqsk3HneKN_eatyzlxVBbQ0v_8VxGp2BvSjvG2BTf4T7hKzY1vdBTSlVrqzrgv46fG0XXUI2BFpS1DEihnhWfF1Y5-QltXaMEhrbOqgMDGjrr0JQPHW4ZHhre8Py6smvCBbcmEwPkeq08vrG1RnGR9S6w4OJ85zavWhcu04KR1R1Y9cp1UR0qGfnF0305Pu3_nEDJUx-F5Se12Wxycj07xVXRgwY_oBHhxd8FhWiWyqBiSJY1pX1bi3lw-cq-feY6BqXVG3SZ6gQErdkeJZTcKHCbcdKSx94ncjLjlAjO_ULD7x0vCDOuTuCmHB1gay4vUBpQsjC5x8KyBt_5A8Bi7FZ0kv-wCCVjA9RbdyfutCm_WDCovQLdlFhtYFXHOy8TCwT8r8f2a9zh09zgjXHI7qO7wumhhr_dw_dceyGImboEi1yvXGBteNanLRFPlTlYUImY
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "chart.labels" -}}
helm.sh/chart: {{ include "chart.chart" . }}
app: {{ .Release.Name }}
version: {{ .Chart.AppVersion | quote }}
{{ include "chart.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "chart.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "chart.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "chart.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

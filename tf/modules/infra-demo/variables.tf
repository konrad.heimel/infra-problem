variable "namespace" {
  type = string
}

variable "newsfeed_secret_name" {
  type = string
  default = "newsfeed-secret"
}

variable "newsfeed_service_token" {
  type      = string
  sensitive = true
}
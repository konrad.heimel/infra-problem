resource "kubernetes_namespace" "infra-demo" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_secret" "newsfeed_secret" {
  metadata {
    name = var.newsfeed_secret_name
    namespace = var.namespace
  }
  data = {
    NEWSFEED_SERVICE_TOKEN=var.newsfeed_service_token
  }
}

resource "helm_release" "argocd-resources" {
  name             = "argocd-resources"
  namespace = "argocd"
  chart = "${path.module}/charts/argocd-resources"
}

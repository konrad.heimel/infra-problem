terraform {
  backend "http" {
  }

  required_providers {
        azurerm = {
            source = "hashicorp/azurerm"
        }
        random = {
            source = "hashicorp/random"
        }
    }
}

provider "azurerm" {
    features {}
    skip_provider_registration = true
}

provider "kubernetes" {
  host     = module.aks.host

  client_certificate     = base64decode(module.aks.client_certificate)
  client_key             = base64decode(module.aks.client_key)
  cluster_ca_certificate = base64decode(module.aks.cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host     = module.aks.host

    client_certificate     = base64decode(module.aks.client_certificate)
    client_key             = base64decode(module.aks.client_key)
    cluster_ca_certificate = base64decode(module.aks.cluster_ca_certificate)
  }
}

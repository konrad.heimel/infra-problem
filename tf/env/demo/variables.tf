# Infra Demo Options
variable "newsfeed_service_token" {
  type      = string
  sensitive = true
}

# Azure Options
variable "azure_region" {
  description = "The location/region where the resources are created."
}

variable "azure_rg_name" {
  description = "Specify the name of the new resource group"
}

variable "source_address_prefix" {
  description = "Provide an IP or subnet to restrict access. 1.2.3.4 or 1.2.3.0/24"
}

variable "dns_prefix" {
  description = "DNS prefix"
}

# Variablen fuer Kubernetes
variable "cluster_name" {
  description = "Name of the kubernetes cluster"
}

variable "agent_count" {
  description = "Number of kubernetes agents"
}

variable "participant_count" {
  default = 8
  type = number
}
variable log_analytics_workspace_name {
  description = "Workspace for log analytics"
}

# refer https://azure.microsoft.com/global-infrastructure/services/?products=monitor for log analytics available regions
variable log_analytics_workspace_location {
  description = "Workspace location for log analytics"
}

# refer https://azure.microsoft.com/pricing/details/monitor/ for log analytics pricing
variable log_analytics_workspace_sku {
  description = "Workspace space for log analytics"
}

# Required tags
variable "tag_customer" {
  description = "Customer name"
}

variable "tag_project" {
  description = "Identify the project"
}

variable "tag_dept" {
  description = "Dept."
}

variable "tag_contact" {
  description = "Email address for project owner"
}

variable "tag_application" {
  description = "What app does this run"
}

variable "tag_version" {
  description = "Version number of this code"
}

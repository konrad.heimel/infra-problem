resource "azurerm_resource_group" "infra-demo" {
  name     = var.azure_rg_name
  location = var.azure_region
}

module "aks" {
  source = "../../modules/aks"

  agent_count                      = var.agent_count
  azure_region                     = var.azure_region
  azure_rg_name                    = var.azure_rg_name
  cluster_name                     = var.cluster_name
  log_analytics_workspace_location = var.log_analytics_workspace_location
  log_analytics_workspace_name     = var.log_analytics_workspace_name
  log_analytics_workspace_sku      = var.log_analytics_workspace_sku
  source_address_prefix            = var.source_address_prefix
  tag_application                  = var.tag_application
  tag_contact                      = var.tag_contact
  tag_customer                     = var.tag_customer
  tag_dept                         = var.tag_dept
  tag_project                      = var.tag_project
  tag_version                      = var.tag_version
}

module "cluster-infrastructure" {
  source = "../../modules/cluster-infrastructure"
}

module "infra-demo" {
  source = "../../modules/infra-demo"
  namespace = "infra-problem-test"
  newsfeed_service_token = var.newsfeed_service_token
}
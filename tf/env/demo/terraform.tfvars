# Name of the kubernetes cluster
cluster_name = "infra-problem-test"

# Number of Nodes
agent_count = 1

# Azure location/region for all resources
azure_region = "westeurope"

# Azure resource group name prefix
azure_rg_name = "InfraDemo"

# Source address prefix for the security policy
source_address_prefix = "*"

# DNS prefix
dns_prefix = "k8s"

# Log analytics workspace name
log_analytics_workspace_name = "LogAnalyticsWorkspace"

# refer https://azure.microsoft.com/global-infrastructure/services/?products=monitor for log analytics available regions
log_analytics_workspace_location = "westeurope"

# refer https://azure.microsoft.com/pricing/details/monitor/ for log analytics pricing
log_analytics_workspace_sku = "PerGB2018"


# Tags
tag_customer = "Thoughtworks"
tag_project = "Kubernetes Training"
tag_dept = "DevOps"
tag_contact = "konrad.heimel@gmail.com"
tag_application = "K8s"
tag_version = "0.9"
